package com.example.cheggtask;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.cheggtask.util.NullCheck;

public class CustomLayout extends ViewGroup {

	private final static String INVALID_NUM_OF_CHILDREN_EXCEPTION = "CustomLayout can only have three views.";
	private final static String TEXT_VIEW_EXCEPTION = "TextView not found!";
	private final static String VIEW1_EXCEPTION = "View1 Not Found.";
	private final static String VIEW2_EXCEPTION = "View2 Not Found.";
	private final static String FIRST_CHILD_NOT_TEXTVIEW_EXCEPTION = "First Child should be a TextView.";

	private final static int TEXTVIEW_INDEX = 0;
	private final static int VIEW1_INDEX = 1;
	private final static int VIEW2_INDEX = 2;

	private final static int TOTAL_NUMBER_OF_VIEWS = 3;

	private ViewHolder mViewHolder;

	public CustomLayout(Context context) {
		this(context, null);
	}

	public CustomLayout(Context context, AttributeSet attrs) {
		super(context, attrs);

		if (attrs == null) {
			LayoutInflater.from(context).inflate(R.layout.layout_custom, CustomLayout.this,
					true);
			onFinishInflate();
		}
	}

	@Override
	public void onFinishInflate() {
		mViewHolder = new ViewHolder(CustomLayout.this);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int layoutWidth = CustomLayout.this.getMeasuredWidth();
		int childCount = getChildCount();
		int childWidthSum = 0;

		for (int i = 0; i < childCount; i++) {
			final View child = getChildAt(i);
			if (child.getVisibility() != GONE) {
				measureChild(child, widthMeasureSpec, heightMeasureSpec);
				childWidthSum += child.getMeasuredWidth();
			}
		}

		int textViewWidth = mViewHolder.mTextView.getMeasuredWidth();
		int maxTextViewWidth = layoutWidth -
				(mViewHolder.mView1.getMeasuredWidth() + mViewHolder.mView2.getMeasuredWidth());

		if (textViewWidth > maxTextViewWidth) {
			// Text is long, in this case there's a need to truncate it, view1 &
			// view2 are touching
			mViewHolder.mTextView.setWidth(maxTextViewWidth);
		} else {
			// Text is short, in this case there's a need to seperate view2 from
			// view1
			mViewHolder.mView1.setTag(layoutWidth - childWidthSum);
		}
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		int childCount = getChildCount();
		int left = 0;

		for (int i = 0; i < childCount; i++) {
			final View child = getChildAt(i);
			final int childWidth = child.getMeasuredWidth();
			final int childHeight = child.getMeasuredHeight();
			final int seperator = (child.getTag() != null) ? ((Integer) child.getTag()) : 0;

			if (child.getVisibility() != GONE) {
				child.layout(left, 0, left + childWidth, childHeight);
				left += childWidth + seperator;
			}
		}
	}

	@Override
	public int getChildCount() {
		int count = super.getChildCount();
		if (count > TOTAL_NUMBER_OF_VIEWS)
			throw new RuntimeException(INVALID_NUM_OF_CHILDREN_EXCEPTION);
		return count;
	}

	public void setView1Dimension(int width, int height) {
		setViewDimension(mViewHolder.mView1, width, height);
	}

	public void setView2Dimension(int width, int height) {
		setViewDimension(mViewHolder.mView2, width, height);
	}

	public void setText(String text) {
		mViewHolder.mTextView.setText(text);
		invalidate();
	}

	private void setViewDimension(View v, int width, int height) {
		v.getLayoutParams().height = height;
		v.getLayoutParams().width = width;
		invalidate();
	}

	class ViewHolder {
		private TextView mTextView;
		private View mView1;
		private View mView2;

		public ViewHolder(ViewGroup v) {
			mTextView = (TextView) getChildAt(TEXTVIEW_INDEX);
			mView1 = getChildAt(VIEW1_INDEX);
			mView2 = getChildAt(VIEW2_INDEX);

			nullCheck();
			initTextView();
		}

		private void nullCheck() {
			NullCheck.throwRuntimeExceptionIfNull(mTextView, TEXT_VIEW_EXCEPTION);
			NullCheck.throwRuntimeExceptionIfNull(mView1, VIEW1_EXCEPTION);
			NullCheck.throwRuntimeExceptionIfNull(mView2, VIEW2_EXCEPTION);
		}

		private void initTextView() {
			if (!(mTextView instanceof TextView)) {
				throw new RuntimeException(FIRST_CHILD_NOT_TEXTVIEW_EXCEPTION);
			}

			mTextView.setEllipsize(TextUtils.TruncateAt.END);
			mTextView.setSingleLine(true);
		}
	}
}
