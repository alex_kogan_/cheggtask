package com.example.cheggtask.util;

import android.view.View;

public class NullCheck {

	public static void throwRuntimeExceptionIfNull(View v, String msg) {
		if (v == null)
			throw new RuntimeException(msg);
	}
}
