package com.example.cheggtask.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

import com.example.cheggtask.R;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

		findViewById(R.id.button_code).setOnClickListener(this);
		findViewById(R.id.button_xml).setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		Class<?> cls = null;
		switch (v.getId()) {
		case R.id.button_code:
			cls = CodeActivity.class;
			break;
		case R.id.button_xml:
			cls = XmlActivity.class;
			break;
		}

		if (cls != null) {
			Intent intent = new Intent(MainActivity.this, cls);
			startActivity(intent);
		}
	}
}
