package com.example.cheggtask.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.cheggtask.CustomLayout;
import com.example.cheggtask.R;

public class XmlActivity extends Activity {

	private CustomLayout mLayout;
	private TextView mTextView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_custom);
		mLayout = (CustomLayout) findViewById(R.id.layout);
		mTextView = (TextView) findViewById(R.id.textView);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mLayout.setText("Always do your best. What you plant now, you will harvest later");
		mLayout.setView1Dimension(200, 100);
		mLayout.setView2Dimension(100, 200);
	}
}
