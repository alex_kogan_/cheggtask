package com.example.cheggtask.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.example.cheggtask.CustomLayout;
import com.example.cheggtask.R;

public class CodeActivity extends Activity {

	private CustomLayout mLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mLayout = new CustomLayout(CodeActivity.this);
		RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.MATCH_PARENT);

		mLayout.setText(getResources().getString(R.string.long_string));
		setContentView(mLayout, rlp);
	}

	@Override
	protected void onResume() {
		super.onResume();
		mLayout.setText("Things do not happen. Things are made to happen.");
		mLayout.setView1Dimension(200, 100);
		mLayout.setView2Dimension(100, 200);
	}

}
